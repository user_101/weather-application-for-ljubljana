package com.example.weatherwidget;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This app is not finished. App widget doesn't show weather info.
 * See: http://stackoverflow.com/questions/33203031/appwidget-wont-update-values-calculated-on-background-thread
 *
 * This app downloads a source from a webpage (view-source:http://vreme.zajcek.org/koseze/),
 * and extracts temperature, wind speed and the amount of rain using regular expressions.
 * Displays the info.
 *
 * */

public class MainActivity extends AppCompatActivity {
    private static String fullPageSource, line;
    private String link = "http://vreme.zajcek.org/koseze/";

    private static final String DEFAULT = "N/A";
    private static final String REFRESH_TIME = "refresh_time";
    private static final String TEMPERATURE = "temperature";
    private static final String WINDSPEED = "windspeed";
    private static final String DOWNFALL = "downfall";

    private String temperatureResult ="empty";
    private String windSpeedResult ="empty";
    private String downfallInLastHoResult ="empty";

    private TextView txtViewLastRefresh, txtViewTemperature, txtViewWindSpeed, txtViewWindDownfallLastH;
    private Button btnRefresh;
    private ProgressBar downloadSourceProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // If there is no SharedPreferences file create one with value: empty
        String s = null;
        s = loadFromSharedP(REFRESH_TIME);
        if(s == "N/A"){
            saveToSharedP("empty", REFRESH_TIME);
        }

        txtViewLastRefresh = (TextView) findViewById(R.id.textViewLastRefresh);
        txtViewTemperature = (TextView) findViewById(R.id.textViewTemperature);
        txtViewWindSpeed = (TextView) findViewById(R.id.textViewWindSpeed);
        txtViewWindDownfallLastH = (TextView) findViewById(R.id.textViewDownfall);

        HandleClick handleClick = new HandleClick();
        btnRefresh = (Button) findViewById(R.id.btnRefresh);
        btnRefresh.setOnClickListener(handleClick);

        if(!isNetworkAvailable()){
            txtViewLastRefresh.setText("No internet connection");
        }

    }

    private class HandleClick implements View.OnClickListener {
        public void onClick(View view) {
            switch(view.getId()) {
                case R.id.btnRefresh:
                    boolean setText = checkRefreshFrequency(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime()));
                    if(setText) {
                        txtViewLastRefresh.setText(getTimeStamp());
                        //saveToSharedP(getTimeStamp(),REFRESH_TIME);
                        new MyNetworkTaskMain().execute(link);
                    }
                    else
                        txtViewLastRefresh.setText("must be 5 min gap between refreshes");
                        txtViewWindSpeed.setText(loadFromSharedP(WINDSPEED));
                        txtViewTemperature.setText(loadFromSharedP(TEMPERATURE));
                        txtViewWindDownfallLastH.setText(loadFromSharedP(DOWNFALL));
                    break;
            }
        }
    }

    public class MyNetworkTaskMain extends AsyncTask<String, Void, Void> {


        String line, fullPageSource;

        @Override
        protected void onPreExecute() {
            downloadSourceProgressBar = (ProgressBar) findViewById(R.id.progressBar);
            downloadSourceProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(String... params) {

            URL url;
            HttpURLConnection httpURLConnection;
            int response = 0;
            InputStream in = null;
            InputStreamReader isr = null;
            BufferedReader br = null;
            Bitmap imageBmp = null;

            try {
                url = new URL(params[0]);
                httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.connect();
                response = httpURLConnection.getResponseCode();
                if (response == httpURLConnection.HTTP_OK) {
                    Log.d("SEBA", "response = httpURLConnection.HTTP_OK");
                    in = httpURLConnection.getInputStream();
                    isr = new InputStreamReader(in);
                    br = new BufferedReader(isr);

                    while ((line = br.readLine()) != null) {

                        //parse temperature
                        if(line.contains("temperatura") && line.contains("C"))
                        {
                            temperatureResult = parseTemperature(line);
                            Log.d("SEBA", "Temperature: " + temperatureResult);

                        }

                        //parse WindSpeed
                        if(line.contains("km/h") && line.contains("m/s") )
                        {
                            windSpeedResult=parseWindSpeed(line);
                            Log.d("SEBA", "WindSpeed: " + windSpeedResult);
                        }

                        //parse Downfall in last hour
                        if(line.contains("padavine v zadnji uri:"))
                        {
                            downfallInLastHoResult = parseDownfallLastHour(line);
                            Log.d("SEBA", "Downfall: " + downfallInLastHoResult);
                        }


                        fullPageSource +=  (line + "\n");
                    }
                }

                isr.close();
                br.close();
                in.close();
                httpURLConnection.disconnect();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            txtViewTemperature.setText(temperatureResult);
            txtViewWindSpeed.setText(windSpeedResult);
            txtViewWindDownfallLastH.setText(downfallInLastHoResult);

            downloadSourceProgressBar.setVisibility(View.GONE);
            saveToSharedP(temperatureResult, TEMPERATURE);
            saveToSharedP(windSpeedResult, WINDSPEED);
            saveToSharedP(downfallInLastHoResult, DOWNFALL);

        }
    }

    public String getTimeStamp(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentTimeStamp = dateFormat.format(new Date());
        return currentTimeStamp;
    }

    /*using regular expressions to parse temperature*/
    public static String parseTemperature(String line){
        float f;
        String strFloat="";

        Pattern SearchPattern = Pattern.compile("\\d*\\.\\d"); //any digit of length 0 or more; followed by dot(.); and any digit

        //String to be searched
//        Matcher MatchString = SearchPattern.matcher("<td>temperatura: </td><td class=dat>7.7 &deg;C</td>");
        Matcher MatchString = SearchPattern.matcher(line);
        while(MatchString.find()) {
           strFloat = MatchString.group();
        }
        return strFloat;
    }

    /*using regular expressions to parse Wind Speed*/
    public static String parseWindSpeed(String line){
        String windSpeed="";

        Pattern SearchPattern = Pattern.compile("\\d*\\.\\d\\s"); //any digit of length 0 or more; followed by dot(.); and any digit; and any space

        Matcher MatchString = SearchPattern.matcher(line);
        while(MatchString.find()) {
            windSpeed = MatchString.group();
        }
        Log.d("SEBA","Windspeed: " + windSpeed );
        return windSpeed;
    }

    /*using regular expressions to parse Downfall in Last Hour*/
    public static String parseDownfallLastHour(String line){

        Log.d("SEBA", "Our line: " + line);
        float f;
        String strFloat="";

        Pattern SearchPattern = Pattern.compile("\\d*\\.\\d*"); //any digit of length 0 or more; followed by dot(.); and any digit; and any space

        Matcher MatchString = SearchPattern.matcher(line);
        while(MatchString.find()) {
            strFloat = MatchString.group();
        }
        return strFloat;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            Toast.makeText(this,"user101-s@yandex.com",Toast.LENGTH_LONG).show();
        }

        if (id == R.id.action_exit) {
            Log.d("SEBA", "Exit");
            finish();
        }

        if (id == R.id.action_send_with_sms) {
            sendWithSMS();
            Log.d("SEBA", "Send with sms");
        }

        return super.onOptionsItemSelected(item);
    }

    //how often the weather info are fetched
    public boolean checkRefreshFrequency(String currentRefreshTime){

        SharedPreferences sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);
        String lastRefreshTime = sharedPreferences.getString("refresh_time", DEFAULT);

        Log.d("SEBA", "currentRefreshTime: " + currentRefreshTime);
        Log.d("SEBA", "lastRefreshTime: " + lastRefreshTime);

        if(lastRefreshTime == "empty"){
            saveToSharedP(currentRefreshTime,REFRESH_TIME);
            return true; // you can refresh
        }
        else {
            saveToSharedP(currentRefreshTime, REFRESH_TIME);
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            Date date1 = new Date();
            Date date2 = new Date();
            try {
                date1 = df.parse(lastRefreshTime);
                date2 = df.parse(currentRefreshTime);
            } catch (ParseException ex) {
                Log.d("SEBA", "Parse error");
            }

            long diff = date2.getTime() - date1.getTime();
            long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);

            if (Math.abs(minutes) >= 6) // diff between last and current refresh is 6 minutes
                return true;
            else return false; // you can't refresh
        }
     }

    public void saveToSharedP(String timeToSave, String key){
        SharedPreferences sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor= sharedPreferences.edit();

        editor.putString(key, timeToSave);

        editor.commit();
    }

    public String loadFromSharedP(String key){
        String value;

        SharedPreferences sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);
        value = sharedPreferences.getString(key, DEFAULT);
        return value;
    }

    //send data with sms
    public void sendWithSMS(){
        Intent intent;
        Intent chooser;

        intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("sms:"));
        intent.putExtra("sms_body", "Weather information: \n"
                                    + "Temperature: " +loadFromSharedP(TEMPERATURE) +"\n"
                                    + "Windspeed: "+ loadFromSharedP(WINDSPEED)+"\n"
                                    + "Downfall: " + loadFromSharedP(DOWNFALL) +"\n");
        intent.setType("vnd.android-dir/mms-sms");
        chooser = intent.createChooser(intent,"Send sms");
        startActivity(chooser);
    }

    private boolean isNetworkAvailable(){
            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

            if(networkInfo != null && networkInfo.isConnected()){
                return true;  // device is connected
            }

            Log.d("SEBA", "Device is not connected to network");
            return false; // // device is not connected
    }

    // to exit app
    @Override
    protected void onDestroy() {
        android.os.Process.killProcess(android.os.Process.myPid());
        super.onDestroy();
    }

}

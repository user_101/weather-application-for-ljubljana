package com.example.weatherwidget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaActionSound;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.RemoteViews;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class MyWidget extends AppWidgetProvider {
    String linkVreme = "http://vreme.zajcek.org/koseze/";
    private RemoteViews views;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);

        for (int i = 0; i < appWidgetIds.length; i++) {

            views = new RemoteViews(context.getPackageName(), R.layout.my_widget_layout);
            String timeNow = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());//String.valueOf(MainActivity.throwDice());

            MyNetworkTask myNetworkTask = new MyNetworkTask();
            myNetworkTask.execute(linkVreme);

            Log.d("SEBA", "Temp: " + String.valueOf(myNetworkTask.temperatureResult));
            views.setTextViewText(R.id.textViewWidget, String.valueOf(myNetworkTask.temperatureResult) + " " + timeNow);

            appWidgetManager.updateAppWidget(appWidgetIds[i], views);

        }
    }
        public class MyNetworkTask extends AsyncTask<String, Void, Void> {

            public String temperatureResult = "empty";
            String line, fullPageSource;

            @Override
            protected Void doInBackground(String... params) {

                URL url;
                HttpURLConnection httpURLConnection;
                int response = 0;
                InputStream in = null;
                InputStreamReader isr = null;
                BufferedReader br = null;
                Bitmap imageBmp = null;

                try {
                    url = new URL(params[0]);
                    httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("GET");
                    httpURLConnection.connect();
                    response = httpURLConnection.getResponseCode();
                    if (response == httpURLConnection.HTTP_OK) {
                        Log.d("SEBA", "response = httpURLConnection.HTTP_OK");
                        in = httpURLConnection.getInputStream();
                        isr = new InputStreamReader(in);
                        br = new BufferedReader(isr);

                        while ((line = br.readLine()) != null) {
                            if(line.contains("temperatura") && line.contains("C"))
                            {
                                Log.d("SEBA", "Temperature: " + MainActivity.parseTemperature(line));
                                temperatureResult = MainActivity.parseTemperature(line);
                            }
                            fullPageSource +=  (line + "\n");
                        }
                    }

                    isr.close();
                    br.close();
                    in.close();
                    httpURLConnection.disconnect();

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                views.setTextViewText(R.id.textViewWidget, String.valueOf(temperatureResult) + " " + new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime()));

            }
        }
}
